﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SowthworksApp.Models
{
    public class SouthworksEvent
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public DateTime StartingDate  { get; set; }
        public string Technology { get; set; }
        public string RistrationLink { get; set; }

        public static List<SouthworksEvent> CreateSouthworksEventListAuto() 
        { 
            List<SouthworksEvent> list = new List<SouthworksEvent>();
            for (int i = 0; i < 1000; i++)
            {
                SouthworksEvent e = new SouthworksEvent()
                {
                    Title = string.Format("Auto event {0}", i),
                    Technology = "Tech",
                    StartingDate = DateTime.Now
                };
                list.Add(e);
            }
            return list;
        }
    }

    public class SouthworksEventDBContext : DbContext
    {
        public DbSet<SouthworksEvent> Events { get; set; }
    }
}