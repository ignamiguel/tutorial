﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SowthworksApp.Models;

namespace SowthworksApp.Controllers
{
    public class SouthworksEventController : Controller
    {
        private SouthworksEventDBContext db = new SouthworksEventDBContext();

        //
        // GET: /Events/

        public ActionResult Index()
        {
            return View(db.Events.ToList());           
        }

        [OutputCache(CacheProfile = "ClientCacheEvents")]
        public ActionResult Table()
        {
            return View(db.Events.ToList());
        }

        
        //
        // GET: /Events/Details/5

        [OutputCache(CacheProfile = "CacheEvents")]
        public ActionResult Details(int id = 0)
        {
            SouthworksEvent southworksEvent = db.Events.Find(id);
            if (southworksEvent == null)
            {
                return HttpNotFound();
            }
            return View(southworksEvent);
        }

        //
        // GET: /Events/Create
        public ActionResult Create()
        {
            return View();
        }
        //
        // POST: /Events/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SouthworksEvent southworksEvent)
        {
            if (ModelState.IsValid)
            {
                db.Events.Add(southworksEvent);
                db.SaveChanges();
                return RedirectToAction("Table");
            }

            return View(southworksEvent);
        }

        //
        // GET: /Events/Edit/5

        public ActionResult Edit(int id = 0)
        {
            SouthworksEvent southworksEvent = db.Events.Find(id);
            if (southworksEvent == null)
            {
                return HttpNotFound();
            }
            return View(southworksEvent);
        }

        //
        // POST: /Events/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SouthworksEvent southworksEvent)
        {
            if (ModelState.IsValid)
            {
                db.Entry(southworksEvent).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Table");
            }
            return View(southworksEvent);
        }

        //
        // GET: /Events/Delete/5

        public ActionResult Delete(int id = 0)
        {
            SouthworksEvent southworksEvent = db.Events.Find(id);
            if (southworksEvent == null)
            {
                return HttpNotFound();
            }
            return View(southworksEvent);
        }

        //
        // POST: /Events/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SouthworksEvent southworksEvent = db.Events.Find(id);
            db.Events.Remove(southworksEvent);
            db.SaveChanges();
            return RedirectToAction("Table");
        }

        public ActionResult CreateSouthworksEventAuto()
        {            
            return View();
        } 

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateSouthworksEventAuto(SouthworksEvent southworksEven)
        {
            List<SouthworksEvent> list = SouthworksEvent.CreateSouthworksEventListAuto();
            
            if (list == null)
            {
                return HttpNotFound();
            }

            if (ModelState.IsValid)
            {
                foreach (var item in list)
                {
                    db.Events.Add(item);
                    db.SaveChanges();
                }
                return RedirectToAction("Table");           
            }
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}